import os
import io
import numpy as np
from util import load_poscar
from numpy.linalg import inv
from numpy.linalg import det
def calc_constantp():
    step = 0.001 #The same number as in elast_const.m
    fin = 0.003 #The same number as in elast_const.m
    strain = np.arange(0,fin, step)
    choice1 = ['C11', 'C22', 'C33', 'C44', 'C55', 'C66']
    path = os.getcwd()
    coo, N_typat, N_types, cell, type_coo, Names = load_poscar('POSCAR');
    vol = det(cell);
    C = np.zeros((6,6))
    k = 0
    for choice in choice1:
        pwd = os.path.join(path, choice)
        d = open(os.path.join(pwd,"data.stress"), "r")
        data = np.array(np.loadtxt(d, dtype="float").tolist())
        s = data.shape
        data1 = np.zeros((s[0], s[1]))
        for i in range(s[1]):
            data1[:,i] = max(data[:,i]) - data[:,i]        # Normalize the data
        
        data = data1*0.1                                # Convert to GPa
        data = data[0:len(strain),:]
        i = 0
        for t in range(s[1]):
            p = np.polyfit(strain, data[:,i], 1) # Fit by linear law 
            C[k,i] = p[0]
            i+=1
            # Get the elastic constant
        k+=1
        
    
    Bv = ((C[0,0]+C[1,1]+C[2,2]) + 2*(C[0,1]+C[1,2]+C[2,0]))/9;
    Gv = ((C[0,0]+C[1,1]+C[2,2]) - (C[0,1]+C[1,2]+C[2,0]) + 3*(C[3,3]+C[4,4]+C[5,5]))/15;
    Ev = 1/((1/(3*Gv))+(1/(9*Bv)));
    vv  = 0.5*(1-((3*Gv)/(3*Bv+Gv)));
    print(C)
    S = inv(C) # refers to elastic compliance tensor, S is the inverse matrix of C
    # Equations to calculate bulk modulus Kv, shear modulus Gv, Young`s modulus Ev, Poisson`s ratio v using the Reuss method
    Br = 1/((S[0,0]+S[1,1]+S[2,2])+2*(S[0,1]+S[1,2]+S[2,0]))
    Gr = 15/(4*(S[0,0]+S[1,1]+S[2,2])-4*(S[0,1]+S[1,2]+S[2,0])+3*(S[3,3]+S[4,4]+S[5,5]))
    Er = 1/((1/(3*Gr))+(1/(9*Br)))
    vr = 0.5*(1-((3*Gr)/(3*Br+Gr)))

    # Equations to calculate bulk modulus Kv, shear modulus Gv, Young`s modulus Ev, Poisson`s ratio v and Vicker`s hardness
    B = (Bv+Br)/2   # Bulk modulus
    G = (Gv+Gr)/2   # Shear modulus
    E = (Ev+Er)/2   # Young`s modulus
    v = (vv+vr)/2   # Poisson`s ratio
    GK  = G/B     # Pugh`s modulus ratio, shear modulus over bulk modulus

    Hv = 2*pow((G*G*G)/(B*B),0.585)-3   # Equation to calculate Vicker`s hardness by Chen-Niu hardness model

    Kg_old = (pow(B,1.2)*pow((B/G),0.5))/240 #  Calculating the Fracture toughness of 3D solid material

    # New fracture toughness model104*70

    alpha = 1;  # for non-metals
    # alpha = 50; % for pure metals
    vol = vol * 1e-30 / sum(N_typat)
    Kg = alpha*pow(vol,(1/6))*G*1000*pow((B/G),0.5)


    # cd(path); TODO
    # elasticProp = [B G E v GK Hv Kg Kg_old]; TODO
    # save data C B G E v GK Hv Kg Kg_old elasticProp TODO
    
    
    #fclose(all);
    # The final data is in C variable which is 6x6 matrix. 
    # inv(C) calculates the elastic compliance tensor for bulk and shear moduli calculations
