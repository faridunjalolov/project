#!/bin/bash
#SBATCH -p lenovo
#SBATCH -t 54:00:00
#SBATCH -N 1
#SBATCH -n 8
#SBATCH -J Relax
#SBATCH --output=log
#SBATCH --error=err
echo $DIR
cd $DIR


for i in C11 C22 C33 C44 C55 C66
do

for j in 0.0 0.001 0.002 0.003 0.004 0.005 0.006
do

cd $DIR/$i/$j

rm -rf $DIR/$i/$j/run.job.$i.$j
rm -rf KPOINTS


cat << EOF >> $DIR/$i/$j/run.job.$i.$j
#!/bin/bash
#SBATCH -o vasp.out
#SBATCH -p lenovo
#SBATCH -J Elastic-$i-$j
#SBATCH -t 54:00:00
#SBATCH -N 1
#SBATCH -n 8
ulimit -s unlimited

module load intel/mkl-11.2.3  mpi/impi-5.0.3  python/python27 vasp/vasp-5.4.4

cd $DIR/$i/$j
mpirun -n 8 vasp_std > output
EOF

sbatch run.job.$i.$j
echo "RUN $i step $j"

done
done
