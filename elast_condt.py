import os
import shutil

import numpy as np

from util import load_poscar, sum


def elast_const(path):
    filename = os.path.join(path, "POSCAR")
    coo, N_typat, N_types, cell, type_coo, Names = load_poscar(filename)
    coo = np.array(coo)
    fr = open(os.path.join(path, 'job.temp'), 'w')
    job_temp_bash_text = f"#!/bin/sh\n" \
                         "SBATCH -o out\n" \
                         "SBATCH -p lenovo\n" \
                         "SBATCH -J convert\n" \
                         "SBATCH -t 1:00:00\n" \
                         "SBATCH -N 1\n" \
                         "SBATCH -n 1\n"
    fr.writelines(job_temp_bash_text)
    choice1 = ['C11', 'C22', 'C33', 'C44', 'C55', 'C66']
    for k in range(len(choice1)):
        fr.write('cd $DIR\n\n')
        choice = choice1[k]
        step = 0.001
        fin = 0.006
        num_def = fin / step + 1.0
        for d in np.arange(0.0, fin, step):
            d1 = 1 / pow((1 - pow(d, 2)), (1 / 3))
            if choice == 'C11':
                e = [[d, 0, 0], [0, 0, 0], [0, 0, 0]]
            elif choice == 'C22':
                e = [[0, 0, 0], [0, d, 0], [0, 0, 0]]
            elif choice == 'C33':
                e = [[0, 0, 0], [0, 0, 0], [0, 0, d]]
            elif choice == 'C44':
                e = [[0, d, 0], [0, 0, 0], [0, 0, 0]]
            elif choice == 'C55':
                e = [[0, 0, 0], [0, 0, d], [0, 0, 0]]
            elif choice == 'C66':
                e = [[0, 0, 0], [0, 0, 0], [d, 0, 0]]

            cell_new1 = cell + cell * e

            one = [[1, 0, 0], [0, 1, 0], [0, 0, 1]]

            cell_newx = (one + e) * cell[0, :]
            cell_newy = (one + e) * cell[1, :]
            cell_newz = (one + e) * cell[2, :]
            cell_new = np.array([cell_newx, cell_newy, cell_newz])
            FOLD = os.path.join(path, choice, str(d))
            os.makedirs(FOLD)

            f = open(os.path.join(FOLD, 'POSCAR'), 'w+')
            f.write(' Comment\n1.0000\n')
            for i in range(2):
                f.write(' %f\t%f\t%f\n' % (cell_new[i, 0, 0], cell_new[i, 1, 1], cell_new[i, 2, 2]))
            for n in range(N_types):
                f.writelines('%s ' % Names[n])
            f.write('\n')
            for typat in N_typat:
                f.writelines('%d ' % typat)
            f.write('\nDirect\n')
            N_atoms = sum(N_typat)
            for j in range(N_atoms):
                f.write(' %f\t%f\t%f\n' % (coo[j, 0], coo[j, 1], coo[j, 2]))
            shutil.copyfile('INCAR', os.path.join(FOLD, 'INCAR'))
            shutil.copyfile('POTCAR', os.path.join(FOLD, 'POTCAR'))

            if d == 0:
                fr.write('cd %s %s\n' % (choice, str(d)))
            else:
                fr.write('cd ../%s\n' % str(d))
            fr.write(
                'grep "  in kB   " OUTCAR | tail -n 1 | awk ''{print $3,$4,$5,$6,$7,$8}'' >> $DIR/%s/data.stress\n' % choice)
            fr.write('ls -l $DIR/%s | sed ''1~0d'' | awk ''{print $9}'' | head -%s > $DIR/%s/data.strain\n\n' % (
                choice, str(num_def), choice))
            f.close()
