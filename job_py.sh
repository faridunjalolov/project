#!/bin/sh 
#SBATCH -o out 
#SBATCH -p lenovo
#SBATCH -J Automazing
#SBATCH -t 64:00:00 
#SBATCH -N 1
#SBATCH -n 32

module load compilers/intel-15.0.3 intel/mkl-11.2.3 mpi/impi-5.0.3 intel/tbb-4.3.3  python/python39 python/python36

python3 main.py
