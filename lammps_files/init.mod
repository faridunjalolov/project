variable up equal 2.0e-2
units		metal
variable cfac equal 1.0e-4
variable cunits string GPa
variable nevery equal 10
variable nrepeat equal 10
variable nfreq equal ${nevery}*${nrepeat}
variable nthermo equal ${nfreq}
variable nequil equal 10*${nthermo}
variable nrun equal 3*${nthermo}
variable temp equal 300.0
variable timestep equal 0.001
variable mass1 equal 28.06
variable mass2 equal 12.011
variable adiabatic equal 0
variable tdamp equal 0.01
variable seed equal 123457
read_data lammps.data
mass 1 ${mass1}
mass 2 ${mass2}
velocity	all create ${temp} 87287
change_box all triclinic