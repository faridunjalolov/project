import os
import shutil

from elast_condt import elast_const

from util import load_poscar


def create_job_script(out_path, job_id=1):
    """
    Args:
        out_path (str)   -   folder where job script will be created.
        job_id   (str)   -   preferable name of your job in squeue,
        and also the name of the folder for your vasp files.
        As a result in the folder with name 'job_id' will be created job script
    """
    if not job_id:
        job_id = os.path.basename(out_path)
    job_script_text = f"#!/bin/bash\n" \
                      "#SBATCH --nodes=1\n" \
                      "#SBATCH --ntasks=8\n" \
                      "#SBATCH --time=06:00:00\n" \
                      "#SBATCH --job-name=Test\n" \
                      "#SBATCH --output=log\n" \
                      "#SBATCH --error=err\n" \
                      "#SBATCH -p lenovo\n" \
                      "module load mpi/impi-5.0.3 intel/mkl-11.2.3 vasp/vasp-5.4.4\n" \
                      "mpirun vasp_std\n" \
                      "mkdir check_dir"
    if not os.path.exists(out_path):
        os.mkdir(out_path)
    with open('%s/jobscript.sh' % out_path, 'w') as job:
        job.writelines(job_script_text)
    INCAR_text = f"ENCUT = 500\n" \
                 "IBRION = 1\n" \
                 "ISIF = 3\n" \
                 "ISMEAR = -5\n" \
                 "LWAVE = .FALSE.\n" \
                 "SIGMA = 0.1\n" \
                 "NSW = 200\n" \
                 "Sigma = 0.01\n" \
                 "ISMEAR = 1\n" \
                 "PREC = Accurate\n" \
                 "KSPACING=0.25"
    with open('%s/INCAR' % out_path, 'w') as job:
        job.writelines(INCAR_text)

    _, _, _, _, _, names = load_poscar("POSCAR")
    txt_all_potcar = ""
    for name in names:
        potcar_file = open("PBE_GGA/%s/POTCAR" % name, "r")
        txt_all_potcar += potcar_file.read()
    with open("POTCAR", "w") as potcar_new:
        potcar_new.writelines(txt_all_potcar)


if __name__ == '__main__':
    create_job_script("./")

    os.system('sbatch jobscript.sh')
    
    while not os.path.exists("check_dir"):
        pass
        
    os.remove("POSCAR")
    os.rename("CONTCAR", "POSCAR")
    name_dft_fold = "1.DFT"
    os.mkdir(name_dft_fold)
    shutil.copyfile('POSCAR', os.path.join(name_dft_fold, 'POSCAR'))
    shutil.copyfile('POTCAR', os.path.join(name_dft_fold, 'POTCAR'))
    shutil.copyfile('create.job.sh', os.path.join(name_dft_fold, 'create.job.sh'))
    shutil.copyfile('elast_condt.py', os.path.join(name_dft_fold, 'elast_condt.py'))
    shutil.copyfile('calc_constantp.py', os.path.join(name_dft_fold, 'calc_constantp.py'))
    INCAR_text_dft = f"PREC = Accurate\n" \
                     "NSW= 30\n" \
                     "IBRION = 2\n" \
                     "ENCUT = 500\n" \
                     "EDIFF = 1.0e-09\n" \
                     "ISMEAR = 0\n" \
                     "SIGMA = 0.01\n" \
                     "IALGO = 48\n" \
                     "LREAL = .FALSE.\n" \
                     "ADDGRID = .TRUE.\n" \
                     "LWAVE = .FALSE.\n" \
                     "LCHARG = .FALSE.\n" \
                     "LREAL=.FALSE.\n" \
                     "KSPACING=0.2"
    with open('%s/INCAR' % name_dft_fold, 'w') as job:
        job.writelines(INCAR_text_dft)
        
    elast_const(name_dft_fold)
    shutil.copyfile('POTCAR', os.path.join(name_dft_fold, 'POTCAR'))
    pwd = os.getcwd()
    dir = os.path.join(pwd, "")
    os.environ["DIR"] = dir
    
    #os.system('sbatch '+name_dft_focdld+'/create.job.sh' + dir)
    #while not os.path.exists("check_dir_0"):
        #pass
    #shutil.copyfile('job.temp', os.path.join(name_dft_fold, 'job.temp'))
    #os.system("sbatch "+name_dft_fold+"/job.temp " + dir)
    #from calc_constantp import calc_constantp
    #calc_constantp()
    
    
    
    name_aimd_fold = "2.AIMD"
    os.mkdir(name_aimd_fold)
    shutil.copyfile('POSCAR', os.path.join(name_aimd_fold, 'POSCAR'))
    shutil.copyfile('POTCAR', os.path.join(name_aimd_fold, 'POTCAR'))
    INCAR_text_aimd = f"LWAVE = .FALSE.\n" \
                     "ENCUT = 500\n" \
                     "System = singlepoint\n" \
                     "LCHARG = .FALSE.\n"\
                     "LREAL= Auto\n"\
                     "NCORE= 8\n"\
                     "ALGO = very fast\n"\
                     "MDALGO = 3\n"\
                     "NWRITE = 2 \n"\
                     "ADDGRID = .TRUE.\n"\
                     "PREC= Accurate\n"\
                     "IBRION = 0\n"\
                     "NSW = 1000\n"\
                     "POTIM = 1.0\n"\
                     "TEBEG = 400\n"\
                     "TEEND = 400\n"\
                     "ISIF=2\n"\
                     "LANGEVIN_GAMMA = 100.0   100.0   100.0\n"\
                     "LANGEVIN_GAMMA_L = 100.0\n"\
                     "PSTRESS = 0\n"\
                     "MAXMIX = 40\n"\
                     "ISYM = 0\n"\
                     "ISMEAR = 0\n"\
                     "SIGMA = 0.05\n"\
                     "SMASS=0\n"\
                     "KSPACING=0.2"
    with open('%s/INCAR' % name_aimd_fold, 'w') as job:
        job.writelines(INCAR_text_aimd)
    new_jobscript_text_aimd = f"#!/bin/bash\n" \
                      "#SBATCH --nodes=1\n" \
                      "#SBATCH --ntasks=32\n" \
                      "#SBATCH --time=48:00:00\n" \
                      "#SBATCH --job-name=aimd\n" \
                      "#SBATCH --output=log\n" \
                      "#SBATCH --error=err\n" \
                      "#SBATCH -p lenovo\n" \
                      "module load mpi/impi-5.0.3 intel/mkl-11.2.3 vasp/vasp-5.4.4\n" \
                       "ulimit -s unlimited\n"\
                      "mpirun vasp_std\n" \
                      "mkdir check_dir"
    with open('%s/jobscript' % name_aimd_fold, 'w') as job:
        job.writelines(new_jobscript_text_aimd)
    os.system('sbatch '+name_aimd_fold+'/jobscript')
    while not os.path.exists(name_aimd_fold+"/check_dir"):
        pass
    name_jobs_fold = "jobs"
    shutil.copyfile(name_jobs_fold+'/job_convert.run', os.path.join(name_aimd_fold, 'job_convert.run'))
    os.system('sbatch '+name_aimd_fold+'/job_convert.run' + dir)
    while not os.path.exists(name_aimd_fold+"/check_dir_1"):
        pass
    name_training_fold = "3.Training"
    os.mkdir(name_training_fold)
    shutil.copyfile(name_aimd_fold+'/output.cfg', os.path.join(name_training_fold, 'output.cfg'))
    shutil.copyfile(name_jobs_fold+'/job_train.run', os.path.join(name_training_fold, 'job_train.run'))
    name_mtp_fold = "MTP_templates"
    shutil.copyfile(name_mtp_fold+'/20g.mtp', os.path.join(name_training_fold, '20g.mtp'))
    os.system('sbatch '+name_training_fold+'/job_train.run' + dir)
    while not os.path.exists(name_training_fold+"/check_dir"):
        pass
    name_cmd_fold = 'lammps_files'
    shutil.copyfile(name_cmd_fold+'/job32.run', os.path.join(name_training_fold, "job32.run"))
    shutil.copyfile(name_cmd_fold+'/in.elastic', os.path.join(name_training_fold, "in.elastic"))
    shutil.copyfile(name_cmd_fold+'/potential.mod', os.path.join(name_training_fold, "potential.mod"))
    shutil.copyfile(name_cmd_fold+'/displace.mod', os.path.join(name_training_fold, "displace.mod"))
    shutil.copyfile(name_cmd_fold+'/mlip.ini', os.path.join(name_training_fold, "mlip.ini"))
    shutil.copyfile(name_cmd_fold+'/lammps.data', os.path.join(name_training_fold, "lammps.data"))
    init_mod_text = f"variable up equal 2.0e-2\n" \
                      "units		metal\n" \
                      "variable cfac equal 1.0e-4\n" \
                      "variable cunits string GPa\n" \
                      "variable nevery equal 10\n" \
                      "variable nrepeat equal 10\n" \
                      "variable nfreq equal ${nevery}*${nrepeat}\n" \
                      "variable nthermo equal ${nfreq}\n" \
                      "variable nequil equal 10*${nthermo}\n" \
                      "variable nrun equal 3*${nthermo}\n" \
                      "variable temp equal 300.0\n" \
                      "variable timestep equal 0.001\n" \
                      "variable mass1 equal 28.06\n" \
                      "variable mass2 equal 12.011\n" \
                      "variable adiabatic equal 0\n" \
                      "variable tdamp equal 0.01\n" \
                      "variable seed equal 123457\n" \
                      "read_data lammps.data\n" \
                      "mass 1 ${mass1}\n" \
                      "mass 2 ${mass2}\n" \
                      "velocity	all create ${temp} 87287\n" \
                      "change_box all triclinic"
    with open('%s/init.mod' % name_cmd_fold, 'w') as job:
        job.writelines(init_mod_text)
    shutil.copyfile(name_cmd_fold+'/init.mod', os.path.join(name_training_fold, "init.mod"))
    os.system('sbatch '+name_training_fold+'/job32.run' + dir)
    while not os.path.exists(name_training_fold+"/check_dir"):
        pass
    name_training_fold_1 = "3.Training/1"
    os.mkdir(name_training_fold_1)
    shutil.copyfile(name_training_fold+'/sampled.cfg_0', os.path.join(name_training_fold_1, "sampled.cfg_0"))
    shutil.copyfile(name_jobs_fold+'/job_select_add.run', os.path.join(name_training_fold, "job_select_add.run"))
    os.system('sbatch '+name_training_fold+'/job_select_add.run' + dir)
    while not os.path.exists(name_training_fold+"/check_dir_0"):
        pass
    shutil.copyfile(name_training_fold_fold+'/new_selected.cfg', os.path.join(name_training_fold_1, "new_selected.cfg"))
    shutil.copyfile(name_jobs_fold+'/job_convert_poscar.run', os.path.join(name_training_fold_1, "job_convert_poscar.run"))
    os.system('sbatch '+name_training_fold_1+'/job_convert_poscar.run' + dir)
    while not os.path.exists(name_training_fold_1+"/check_dir_1"):
        pass
    shutil.copyfile('POTCAR', os.path.join(name_training_fold_1, 'POTCAR'))
    INCAR_aimd_text = f"PREC = Accurate\n" \
                      "ISTART = 0\n" \
                      "NSW = 999\n" \
                      "IBRION = 2\n" \
                      "ISIF = 2\n" \
                      "ICHARG = 2\n" \
                      "IALGO  = 48\n" \
                      "LREAL= F\n" \
                      "ISYM = 0\n" \
                      "ENCUT = 500.0\n" \
                      "EDIFF = 1e-6\n" \
                      "ISMEAR = 1 ; SIGMA = 0.060\n" \
                      "POTIM = 0.020\n" \
                      "LCHARG = FALSE\n" \
                      "LWAVE = FALSE\n" \
                      "KSPACING=0.2"
    with open('%s/INCAR' % name_training_fold_1, 'w') as job:
        job.writelines(INCAR_aimd_text)
    shutil.copyfile(name_jobs_fold+'/jobscript', os.path.join(name_training_fold_1, "jobscript"))
    os.system('sbatch '+name_training_fold_1+'/jobscript' + dir)
    while not os.path.exists(name_training_fold_1+"/check_dir_2"):
        pass
    shutil.copyfile(name_jobs_fold+'/job_convert.run', os.path.join(name_training_fold_1, "job_convert.run"))
    os.system('sbatch '+name_training_fold_1+'/job_convert.run' + dir)
    while not os.path.exists(name_training_fold_1+"/check_dir_convert"):
        pass
    os.rename(name_training_fold_1+"/ouput.cfg", os.path.join(name_training_fold_1, "output1.cfg"))
    name_training_fold_2 = "3.Training/2"
    os.mkdir(name_training_fold_2)
    shutil.copyfile(name_training_fold_1+'/ouput1.cfg', os.path.join(name_training_fold_2, "ouput1.cfg"))
    shutil.copyfile(name_training_fold+'/ouput.cfg', os.path.join(name_training_fold_2, "ouput.cfg"))
    shutil.copyfile(name_jobs_fold+'/job_cat.run', os.path.join(name_training_fold_2, "job_cat.run"))
    os.system('sbatch '+name_training_fold_2+'/job_cat.run' + dir)
    while not os.path.exists(name_training_fold_2+"/check_dir_cat"):
        pass                      
    shutil.copyfile(name_training_fold+'/trained.mtp', os.path.join(name_training_fold_2, "trained20.mtp"))
    shutil.copyfile(name_jobs_fold+'/job_train_1.run', os.path.join(name_training_fold_2, "job_train_1.run"))
    os.system('sbatch '+name_training_fold_2+'/job_train_1.run' + dir)
    while not os.path.exists(name_training_fold_2+"/check_dir_train1"):
        pass
    shutil.copyfile(name_cmd_fold+'/job32.run', os.path.join(name_training_fold_2, "job32.run"))
    shutil.copyfile(name_cmd_fold+'/in.elastic', os.path.join(name_training_fold_2, "in.elastic"))
    shutil.copyfile(name_cmd_fold+'/potential.mod', os.path.join(name_training_fold_2, "potential.mod"))
    shutil.copyfile(name_cmd_fold+'/displace.mod', os.path.join(name_training_fold_2, "displace.mod"))
    shutil.copyfile(name_cmd_fold+'/lammps.data', os.path.join(name_training_fold_2, "lammps.data"))
    shutil.copyfile(name_cmd_fold+'/mlip.ini', os.path.join(name_training_fold_2, "mlip.ini"))
    shutil.copyfile(name_cmd_fold+'/init.mod', os.path.join(name_training_fold_2, "init.mod"))
    file1 = open(name_training_fold_2+'/mlip.ini', 'w')
    L = ["mlip TRUE\n", "mlip:load_from  updated20.mtp\n", "calculate_efs TRUE\n", "sample  TRUE\n", "sample:threshold 2.0\n", "sample:threshold_break  11.0\n", 
         "sample:save_sampled_to    sampled.cfg\n", "No configur\n"]
    file1.writelines(L)
    file1.close()
    os.system('sbatch '+name_training_fold_2+'/job32.run' + dir)
    while not os.path.exists(name_training_fold_2+"/check_dir_32"):
        pass
    shutil.copyfile(name_jobs_fold+'/job_select_add.run', os.path.join(name_training_fold, "job_select_add.run"))
    os.system('sbatch '+name_training_fold_2+'/job_select_add.run' + dir)
    while not os.path.exists(name_training_fold_2+"/check_dir_select"):
        pass
    name_training_fold_3 = "3.Training/3"
    os.mkdir(name_training_fold_3)
    shutil.copyfile(name_training_fold_2+'/new_selected.cfg', os.path.join(name_training_fold_3, "new_selected.cfg"))
    shutil.copyfile(name_jobs_fold+'/job_convert_poscar.run', os.path.join(name_training_fold_3, "job_convert_poscar.run"))
    os.system('sbatch '+name_training_fold_3+'/job_convert_poscar.run' + dir)
    while not os.path.exists(name_training_fold_3+"/check_dir_convert_poscar"):
        pass
    shutil.copyfile('POTCAR', os.path.join(name_training_fold_3, 'POTCAR'))
    shutil.copyfile(name_training_fold_1+"/INCAR", os.path.join(name_training_fold_3, "INCAR"))
    shutil.copyfile(name_jobs_fold+'/jobscript', os.path.join(name_training_fold_3, "jobscript"))
    os.system('sbatch '+name_training_fold_3+'/jobscript' + dir)
    while not os.path.exists(name_training_fold_3+"/check_dir_job"):
        pass
    shutil.copyfile(name_jobs_fold+'/job_convert.run', os.path.join(name_training_fold_3, "job_convert.run"))
    os.system('sbatch '+name_training_fold_3+'/job_convert.run' + dir)
    while not os.path.exists(name_training_fold_3+"/check_dir_convert"):
        pass
    os.rename(name_training_fold_1+"/ouput.cfg", os.path.join(name_training_fold_1, "output1.cfg"))
    name_training_fold_4 = "3.Training/4"
    os.mkdir(name_training_fold_4)
    shutil.copyfile(name_training_fold_3+'/ouput1.cfg', os.path.join(name_training_fold_4, "ouput1.cfg"))
    shutil.copyfile(name_training_fold_2+'/ouput.cfg', os.path.join(name_training_fold_4, "ouput.cfg"))
    shutil.copyfile(name_jobs_fold+'/job_cat.run', os.path.join(name_training_fold_4, "job_cat.run"))
    os.system('sbatch '+name_training_fold_4+'/job_cat.run' + dir)
    while not os.path.exists(name_training_fold_4+"/check_dir_cat"):
        pass 
    
