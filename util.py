import io

import numpy as np


def sum(arr):
    sum = 0
    for i in range(0, len(arr)):
        sum = sum + int(arr[i])
    return sum


def parse_array_to_float(line):
    return [float(x) for x in line.split()]


def load_poscar(filename):
    f = open(filename, "r")
    tline1 = f.readline()
    tline2 = f.readline()
    alat = float(tline2)
    line1 = f.readline()
    line2 = f.readline()
    line3 = f.readline()
    cell1 = [parse_array_to_float(line1), parse_array_to_float(line2), parse_array_to_float(line3)]
    an_array = np.array(cell1)
    cell = an_array * alat
    line_names_atom = f.readline()
    txt_names_atom = io.StringIO(line_names_atom)
    names_of_Atom = np.loadtxt(txt_names_atom, dtype="str").tolist()
    line4 = f.readline()
    N_typat = parse_array_to_float(line4)
    N_types = len(N_typat)
    type_coo = f.readline()
    coon = []
    for i in range(sum(N_typat)):
        coo = parse_array_to_float(f.readline())
        coon.append([coo[0], coo[1], coo[2]])
    coo = coon
    return coo, N_typat, N_types, cell, type_coo, names_of_Atom
